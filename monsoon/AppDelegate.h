//
//  AppDelegate.h
//  monsoon
//
//  Created by Yashashvi Kampalli (Student) on 3/17/14.
//  Copyright (c) 2014 Yashashvi Kampalli (Student). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
