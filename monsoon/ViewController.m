//
//  ViewController.m
//  monsoon
//
//  Created by Yashashvi Kampalli (Student) on 3/17/14.
//  Copyright (c) 2014 Yashashvi Kampalli (Student). All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //Array that holds the data for the tableview
    mydata=[[NSMutableArray alloc] initWithObjects: nil];
    
    /*Procedure that generates the data for the tableview according to the conditions
    Print numbers from 1 t0 100;
     1) If the number is divisible by 3 should dislay blue
     2) If the number is divisible by 5 should dislay green
     3) If the number is divisible by both 3 and 5 should dislay bluegreen
     In all other cases should just display the number
     */
    
    for (int i=1; i<=100; i++) {
        if (i%15==0) {
            NSLog(@"\nblue green");
            [mydata addObject:@"blue green"];
        }else if(i%3==0){
            NSLog(@"\nblue");
        [mydata addObject:@"blue"];
        }else if(i%5==0){
           [mydata addObject:@"green"];
        }else{
            NSLog(@"%d",i);
            [mydata addObject:[NSString stringWithFormat:@"%d",i]];
        }
    }
    
    //Create and set the table view to the current view and configure its datasource and delegate
    
    myTableview=[[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    myTableview.delegate=self;
    myTableview.dataSource=self;
    [myTableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"newCell"];
    [self.view addSubview:myTableview];
    
    //TO test the second question -> calculating the factorial of an number
    //NSLog(@"factorial of 5 is %d",[self factorial:5]);
    
}

-(int)factorial:(int)n{
    if (n==1) {
        return 1;
    }else{
        return n*[self factorial:n-1];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [mydata count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *currentCell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"newCell"];
    [currentCell.textLabel setText:[mydata objectAtIndex:indexPath.row]];
   // [currentCell.textLabel setText:@"sample"];
    return currentCell;
}
@end
